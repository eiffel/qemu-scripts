# Collection of script to build and run VM

This repository contains a collection of script to build reproducible VM for `amd64` and `armel` architectures.
The VM are based on debian stable.

## Getting started

To build the VM and run it, use the following commands:

```bash
# Create the VM
user@home$ sudo bash create_image.sh
# Run the VM with qemu.
user@home$ bash run_extern_kernel.sh
root@vm-amd64# echo 'From inside the VM'
```

The following command can be used to build and run a i386 VM:

```bash
# Create the VM
user@home$ sudo bash create_image.sh i386
# Run the VM with qemu.
user@home$ bash run_extern_kernel.sh i386
root@vm-i386# echo 'From inside the VM'
```

Use the following commands to build and run an armel VM:

```bash
# Create the VM
user@home$ sudo bash create_image.sh armel
# Run the VM with qemu.
user@home$ bash run_extern_kernel.sh armel
root@vm-armel# echo 'From inside the VM'
```

This commands permits you to build and run an arm64 VM:

```bash
# Create the VM
user@home$ sudo bash create_image.sh arm64
# Run the VM with qemu.
user@home$ bash run_extern_kernel.sh arm64
root@vm-arm64# echo 'From inside the VM'
```

This commands permits you to build and run a powerpc64el VM:

```bash
# Create the VM
user@home$ sudo bash create_image.sh ppc64el
# Run the VM with qemu.
user@home$ bash run_extern_kernel.sh ppc64el
root@vm-arm64# echo 'From inside the VM'
```

## VM creation

The script `create_image.sh` creates a raw qemu image of 25GB and install debian stable in it.
Without giving an argument, the script creates an image for `amd64` architecture.
This script calls `setup_vm.sh` which installs other software and add `share` to fstab.

`share` is a special directory which permits seeing host files from the VM.
The files are read-only and corresponding to the tree above the execution of `run_extern_kernel.sh`.

## Kernel creation

Before running the VM, you need to compile a linux kernel.
A "ready to qemu" linux kernel for `amd64` can be obtained using the following:

```bash
# Define a .config for x86_64.
user@home$ make x86_64_defconfig
# Add virtualisation options.
user@home$ make kvm_guest.config
# Then compile it and wait.
user@home$ make -j$(nproc)
```

For `i386`, you will need to indicate the `ARCH`:

```bash
# Define a .config for i386
user@home$ make ARCH=i386
# Add virtualisation options.
user@home$ make kvm_guest.config
# Then compile it and wait.
user@home$ make ARCH=i386 -j$(nproc)
```

For `armel`, the recipe is the following, note that you will need a cross compiler:

```bash
# Define a .config for arm.
user@home$ make ARCH=arm defconfig
# Add virtualisation options.
user@home$ make ARCH=arm kvm_guest.config
# Then compile it and wait.
user@home$ make ARCH=arm CROSS_COMPILE=arm-linux-gnueabi- -j$(nproc)
```

Like the `armel` recipe, you will need a cross compiler for the `arm64` kernel:

```bash
# Define a .config for arm64.
user@home$ make ARCH=arm64 defconfig
# Add virtualisation options.
user@home$ make ARCH=arm64 kvm_guest.config
# Then compile it and wait.
user@home$ make ARCH=arm64 CROSS_COMPILE=aarch64-linux-gnu- -j$(nproc)
```

Like the `armel` recipe, you will need a cross compiler for the `powerpc` kernel:

```bash
# Define a .config for powerpc.
# pseries are little endian while defconfig will be big endian.
# We need to use the little endian one as the rootfs is little endian since
# debian does not support big endian powerpc.
user@home$ make ARCH=powerpc pseries_defconfig
# Add virtualisation options.
user@home$ make ARCH=powerpc kvm_guest.config
# Then compile it and wait.
user@home$ make ARCH=powerpc CROSS_COMPILE=powerpc64le-linux-gnu- -j$(nproc)
```

## VM run

You are now ready to use `run_extern_kernel.sh` to run your VM!
Maybe you will need to modify the values of `-smp` and `-m` to change the amount of CPU cores and memory available for the VM.
You can also modify the `-cpu` argument to enable CPU specific features.

## Kernel debug

You may want to debug the kernel running inside your VM:
```bash
# You should first have compile your kernel with CONFIG_DEBUG=y.
# You should also set KDB=1 inside run_extern_kernel.sh so ASLR is switched off.
user@home$ bash run_extern_kernel.sh
...
root@vm-amd64#
# Open an other terminal.
user@home$ gdb-multiarch vmlinux
Reading symbols from vmlinux...
# You can now set a breakpoint wherever you want in kernel code.
# For this example, we will stop at exec syscall.
# NOTE If you want to debug syscall (e.g. __x64_sys_perf_event_open), you should
# compile the kernel with CONFIG_X86_X2APIC=y.
# Indeed, this option will force interrupt to be executed by unused cores, so
# you will not be blocked while stepping into the code.
# This remark is only true for x86_64, see:
# https://stackoverflow.com/a/65526544
# https://techlibrary.hpe.com/docs/iss/proliant-gen10-uefi/s_enable_disable_x2APIC_support.html
(gdb) b __x64_sys_execve
Breakpoint 1 at 0xffffffff811cc410: file fs/exec.c, line 1956.
# We can now connect to GDB running inside QEMU.
(gdb) target remote :1234
Remote debugging using :1234
default_idle () at arch/x86/kernel/process.c:581
581             trace_cpu_idle_rcuidle(PWR_EVENT_EXIT, smp_processor_id());
# VM should be stopped, we will continue its execution.
(gdb) c
Continuing.
# Go back to the VM terminal and force the kernel code to be called.
# We just need to run something to get execve being called.
root@vm-amd64# ls -al
# Nothing is printed because VM is stopped.
# Go back to GDB terminal.
[Switching to Thread 1.4]

Thread 4 hit Breakpoint 2, __x64_sys_execve (regs=0xffffc90000427f58) at fs/exec.c:1956
1956    SYSCALL_DEFINE3(execve,
# You can now play GDB like you would do with user software.
(gdb) list
1951                    return;
1952
1953            set_mask_bits(&mm->flags, MMF_DUMPABLE_MASK, value);
1954    }
1955
1956    SYSCALL_DEFINE3(execve,
1957                    const char __user *, filename,
1958                    const char __user *const __user *, argv,
1959                    const char __user *const __user *, envp)
1960    {
# When you found your problem, you remove your breakpoint and continue VM
# execution.
(gdb) d 1
(gdb) c
# ls should have finished its execution.
total 80
drwxr-xr-x  7 root root  4096 Oct 19 16:47 .
drwxr-xr-x 18 root root  4096 Jan  8  2021 ..
-rw-------  1 root root  1626 Oct 19 17:48 .bash_history
-rw-r--r--  1 root root   570 Jan 31  2010 .bashrc
drwx------  3 root root  4096 Jan 22  2021 .gnupg
drwxr-xr-x  3 root root  4096 Jan 22  2021 .local
-rw-r--r--  1 root root   148 Aug 17  2015 .profile
drwxr-xr-x  2 root root  4096 Jan  8  2021 .ssh
drwxr-xr-x  1 1000 1000    70 Oct 19 18:00 share
root@vm-amd64:~#
```

Note that to debug a kernel whom architecture is different than that of your host you will need a cross debugger.
But the process will be almost the same.

## Authors

**Francis Laniel** [<laniel_francis@privacyrequired.com>](laniel_francis@privacyrequired.com)

## License

This project is licensed under the MPLv2 License.