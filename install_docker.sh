#! /usr/bin/env bash
# The recipe is taken from:
# https://docs.docker.com/install/linux/docker-ce/debian/

CONTAINERD_DIR='/etc/systemd/system/containerd.service.d'

if [ $EUID -ne 0 ]; then
	echo "This script must be run as root!" 1>&2

	exit 1
fi

# Try to remove docker if it was previously installed.
apt-get -qy remove docker-ce containerd.io
apt-get -qy autoremove

# Update the repository.
apt-get -qy update

# Install packages to allow apt to use https.
apt-get -qy install apt-transport-https ca-certificates curl gnupg2 software-properties-common

# Add Docker’s official GPG key.
curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add -

# Should print something.
apt-key fingerprint 0EBFCD88

# Add docker repository.
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable"

# Update the repositories to fetch from docker's.
apt-get -qy update

# # First we install linux related packages from stretch-backports.
# apt-get -qy install linux-compiler-gcc-6-x86=4.9.189-3 \
# 	linux-headers-4.9.0-11-amd64=4.9.189-3 \
# 	linux-headers-4.9.0-11-common=4.9.189-3 \
# 	linux-headers-amd64=4.9+80+deb9u9 \
# 	linux-kbuild-4.9=4.9.189-3 \
# 	linux-libc-dev=4.9.189-3

# Then containerd.io.
apt-get -qy install containerd.io=1.2.6-3

# Finally docker-ce-cli and the remaining with docker-ce.
apt-get -qy install docker-ce-cli=5:19.03.2~3-0~debian-stretch docker-ce=5:19.03.2~3-0~debian-stretch

# Install docker-compose. The method can seem strange but it was taken from:
# https://docs.docker.com/compose/install/#install-compose
curl -L "https://github.com/docker/compose/releases/download/1.23.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

# Mark it as executable.
chmod +x /usr/local/bin/docker-compose

# Before running docker we need to change a few things to container.d.
# Indeed, when containerd is launched from systemd it tries to modprobe overlay.
# In our cases overlay is built with the kernel so this modprobe will always
# fail.
# We need to override this behavior. Information was gathered from:
# https://github.com/docker/for-linux/issues/475#issuecomment-437373774
# Firstly we create the containerd directory in systemd directory.

if [ ! -d $CONTAINERD_DIR ]; then
	mkdir $CONTAINERD_DIR
fi

# Then we write an empty ExecStartPre in override.conf to override its default
# value (which is 'modprobe overlay').
echo -e "[Service]\nExecStartPre=" > $CONTAINERD_DIR/override.conf

echo -e "Installation is now finished you should be able to run 'docker run hello-world' without problem!\nIf there is one try first to reboot to correctly start dockerd."