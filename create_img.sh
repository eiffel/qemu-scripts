#! /usr/bin/env bash
# The following recipe was taken from:
# https://www.collabora.com/news-and-blog/blog/2017/01/16/setting-up-qemu-kvm-for-kernel-development/

IMG_FORMAT='qemu-image-%s.img'
DIR='mount-point'

# TODO Add install_docker.sh.
INIT_SCRIPTS='setup_vm.sh'

if [ $EUID -ne 0 ]; then
	echo "${0} must be run as root!" 1>&2

	exit 1
fi

# If no argument is given, default to amd64 architecture.
arch='amd64'

# If an argument is given, set arch to this.
# If the user gave an unknown architecture, qemu-debootstrap will fail.
if [ $# -ge 1 ]; then
	arch=$1
fi

# Add architecture to image name.
printf -v image $IMG_FORMAT $arch

# The argument "mom likes" (or "am i") is a sort of an easter egg who will print
# the user of shell where this script was run. The call to awk permit to clean
# the output of who and just get the username.
real_user=$(who mom likes | awk '{print $1}')

# Create a 25GB raw image so experiment with 8 containers can run without
# problem.
qemu-img create -f raw $image 25G

# # Format the image as ext4.
mkfs.ext4 $image

mkdir $DIR
mount -o loop $image $DIR

# We can now install debian.
# Using qemu-debootstrap permits creating image for other architectures.
# If destination architecture is the same as host, using qemu-debootstrap is the
# same as using debootstrap.
# NOTE In this case, a warning is printed by qemu-debootstrap.
qemu-debootstrap --arch $arch stable $DIR

# Copy some initialization script in the chrooted environment so they can be
# run from it.
for i in $INIT_SCRIPTS; do
	cp $i $DIR
done

# WARNING Redeclare locally the SHELL value to avoid problem with zsh when
# chrooting.
SHELL='/bin/sh'

# Call those scripts.
cat << EOF | chroot $DIR
	for i in $INIT_SCRIPTS; do
		bash $i $arch
	done
EOF

# Clean the scripts previously copied.
for i in $INIT_SCRIPTS; do
	rm $DIR/$i
done

# Configure openssh-server to be able to login as root without password.
mkdir $DIR/root/.ssh
# Add the host public key given to the authorized_keys.
cat /home/$real_user/.ssh/id_rsa.pub >> $DIR/root/.ssh/authorized_keys
# Authorize root login without password.
perl -pi -e 's/#PermitRootLogin prohibit-password/PermitRootLogin without-password/' $DIR/etc/ssh/sshd_config

umount $DIR
rmdir $DIR

# Change the owner of image from root to the real user who uses sudo to run this
# script.
chown $real_user $image

echo -e "Image seems ready!\nYou can now run it with run_extern_kernel.sh!\n"