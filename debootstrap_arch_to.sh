#! /usr/bin/env bash
# Copyright (c) 2020 Francis Laniel <laniel_francis@privacyrequired.com>
# SPDX-License-Identifier: MPL-2.0
# Collection of function used in run_extern_kernel.sh.


# Translates a debootstrap architecture into a kernel architecture.
# E.g. when given 'amd64' as argument, it returns 'x86_64'.
#
# This function is inspired from a snippet found in qemu-debootstrap script.
#
# @param $1 The debootstrap architecture to convert. If no argument is provided,
# the function exits the script and prints a failure message.
# @return Returns the corresponding architecture. If no correspondence
# architecture was found, exit the script and print a failure message.
function debootstrap_arch_to_kernel_arch {
	local deb_arch

	if [ $# -ne 1 ]; then
		echo "${FUNCNAME[0]} needs one argument: the debootstrap architecture" 1>&2

		exit 1
	fi

	deb_arch=$1

	case "${deb_arch}" in
		amd64)
			echo 'x86_64'
		;;
		i386)
			echo 'x86'
		;;
		armel)
			echo 'arm'
		;;
		arm64)
			echo 'arm64'
		;;
		ppc64el)
			echo 'powerpc'
		;;
		*)
			echo "No kernel architecture found for debootstrap architecture '${deb_arch}'!" 1>&2

			exit 1
		;;
	esac
}

# Translates a debootstrap architecture into a kernel architecture.
# E.g. when given 'amd64' as argument, it returns 'x86_64'.
#
# This function is inspired from a snippet found in qemu-debootstrap script.
#
# @param $1 The debootstrap architecture to convert. If no argument is provided,
# the function exits the script and prints a failure message.
# @return Returns the corresponding architecture. If no correspondence
# architecture was found, exit the script and print a failure message.
function debootstrap_arch_to_qemu_arch {
	local deb_arch

	if [ $# -ne 1 ]; then
		echo "${FUNCNAME[0]} needs one argument: the debootstrap architecture" 1>&2

		exit 1
	fi

	deb_arch=$1

	case "${deb_arch}" in
		amd64)
			echo 'x86_64'
		;;
		i386)
			echo 'i386'
		;;
		armel)
			echo 'arm'
		;;
		arm64)
			echo 'aarch64'
		;;
		ppc64el)
			echo 'ppc64'
		;;
		*)
			echo "No qemu architecture found for debootstrap architecture '${deb_arch}'!" 1>&2

			exit 1
		;;
	esac
}

# Translates a debootstrap architecture into a machine qemu argument.
# E.g. when given 'armel' as argument, it returns '-machine virt'.
#
# This function is inspired from a snippet found in qemu-debootstrap script.
#
# @param $1 The debootstrap architecture to convert. If no argument is provided,
# the function exits the script and prints a failure message.
# @return Returns the corresponding machine. For debootstrap architecture
# different from armel and arm64, it returns an empty string so it will use qemu
# default value for -machine option.
function debootstrap_arch_to_machine {
	local deb_arch

	if [ $# -ne 1 ]; then
		echo "${FUNCNAME[0]} needs one argument: the debootstrap architecture" 1>&2

		exit 1
	fi

	deb_arch=$1

	case "${deb_arch}" in
		armel)
			# Use the virt machine for emulation.
			# The virt machine does not exist and its goal is to be used for
			# emulation.
			# highmem=off restrain the fact to map physical addresse above 4G as armel
			# is a 32 bits architecture.
			# Thank you pm215 from qemu@irc.oftc.net for the help!
			echo '-machine virt,highmem=off'
		;;
		arm64)
			# Still use the virt machine!
			echo '-machine virt'
		;;
		ppc64el)
			# ppce500 is designed for virtualization:
			# https://www.qemu.org/docs/master/system/ppc/ppce500.html
			# But it seems easier to use pseries:
			# https://www.qemu.org/docs/master/system/ppc/pseries.html
			# x-vof is a firmware which permits booting faster:
			# https://www.qemu.org/docs/master/system/ppc/pseries.html#firmware
			echo '-machine pseries,x-vof=on'
		;;
		*)
			echo ''
		;;
	esac
}

# Translates a debootstrap architecture into a cpu qemu argument.
# E.g. when given 'arm64' as argument, it returns '-cpu cortex-a53'.
#
# This function is inspired from a snippet found in qemu-debootstrap script.
#
# @param $1 The debootstrap architecture to convert. If no argument is provided,
# the function exits the script and prints a failure message.
# @return Returns the corresponding machine. For debootstrap architecture
# different from arm64, it returns an empty string so it will use qemu default
# value for -cpu option.
function debootstrap_arch_to_cpu {
	local deb_arch

	if [ $# -ne 1 ]; then
		echo "${FUNCNAME[0]} needs one argument: the debootstrap architecture" 1>&2

		exit 1
	fi

	deb_arch=$1

	case "${deb_arch}" in
		arm64)
			# arm64 needs a CPU, so we set it to Cortex A53.
			# There are a lot of CPU which can be got with -cpu help, but I choose the
			# A53 as default.
			echo '-cpu cortex-a53'
		;;
		*)
			echo ''
		;;
	esac
}

# Translates a debootstrap architecture into a boot image.
# E.g. when given 'amd64' as argument, it returns 'bzImage'.
#
# @param $1 The debootstrap architecture to convert. If no argument is provided,
# the function exits the script and prints a failure message.
# @return Returns the corresponding boot image. If no correspondence image was
# found, exits the script and prints a failure message.
function debootstrap_arch_to_boot_image {
	local deb_arch

	if [ $# -ne 1 ]; then
		echo "${FUNCNAME[0]} needs one argument: the debootstrap architecture" 1>&2

		exit 1
	fi

	deb_arch=$1

	case "${deb_arch}" in
		amd64|i386)
			# Default make for X86_64 kernel will create bzImage.
			echo 'bzImage'
		;;
		armel|ppc64el)
			# Default make for arm/powerpc kernel will create zImage.
			echo 'zImage'
		;;
		arm64)
			# Default make for arm64 kernel will create Image.
			echo 'Image'
		;;
		*)
			echo "No boot image for debootstrap architecture '${deb_arch}'!" 1>&2

			exit 1
		;;
	esac
}

# Translates a debootstrap architecture into device qemu argument.
# E.g. when given 'amd64' as argument, it returns 'bzImage'.
#
# @param $1 The debootstrap architecture to convert. If no argument is provided,
# the function exits the script and prints a failure message.
# @return Returns the corresponding device. If no corresponding device was
# found, exits the script and prints a failure message.
function debootstrap_arch_to_device {
		local deb_arch

	if [ $# -ne 1 ]; then
		echo "${FUNCNAME[0]} needs one argument: the debootstrap architecture" 1>&2

		exit 1
	fi

	deb_arch=$1

	case "${deb_arch}" in
		amd64|ppc64el|i386)
			# Indicates that hdd is an scsi device.
			# All documentation on scsi related options was found here:
			# https://fedoraproject.org/wiki/Features/virtio-scsi
			echo '-device virtio-blk-pci,drive=hdd'
		;;
		arm64|armel)
			# For armel and arm64, it seems that hdd has to be an virtio-blk-device
			# which is accessed through virtio.
			# So, in this case, the kernel has to be configured with
			# CONFIG_VIRTIO_*=y.
			echo '-device virtio-blk-device,drive=hdd'
		;;
		*)
			echo "No device for debootstrap architecture '${deb_arch}'!" 1>&2

			exit 1
		;;
	esac
}

# Translates a debootstrap architecture into kernel command line.
# E.g. when given 'amd64' as argument, it returns 'root=/dev/sda rw console=ttyS0 log_buf_len=50M'.
#
# @param $1 The debootstrap architecture to convert. If no argument is provided,
# the function exits the script and prints a failure message.
# @return Returns the corresponding kernel command line. If no corresponding
# kernel command line was found, exits the script and prints a failure message.
function debootstrap_arch_to_kernel_command_line {
		local deb_arch

	if [ $# -ne 1 ]; then
		echo "${FUNCNAME[0]} needs one argument: the debootstrap architecture" 1>&2

		exit 1
	fi

	deb_arch=$1

	case "${deb_arch}" in
		amd64|i386)
			# Since qemu-img-amd64.img contains one partition whom type is ext4 we
			# need to give it as root.
			# The device used is /dev/sda because we use scsi.
			# NOTE If something does not work it can be good to tweak /dev/sda to /dev/sda1
			# or /dev/vda and /dev/vda1.
			# WARNING It is also a good thing to explicitly write the 'rw' so the
			# partition can be written.
			# The console option is to print directly the output of qemu in the
			# terminal.
			echo 'root=/dev/vda rw console=ttyS0 log_buf_len=50M'
		;;
		arm64|armel|ppc64el)
			# For qemu-img-armel.img, it also contains an ext4 partition used as root.
			# But, we use /dev/vda because the image is used with virtio-blk-device.
			# We do not need to give a value to console to print directly the ouput
			# inside the terminal.
			# The same applies for ppc64el despite using virtio-blk-pci.
			echo 'root=/dev/vda rw log_buf_len=50M'
		;;
		*)
			echo "No command line for debootstrap architecture '${deb_arch}'!" 1>&2

			exit 1
		;;
	esac
}