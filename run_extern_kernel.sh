#! /usr/bin/env bash
# Script based on the script given in PNL and some options from:
# https://www.collabora.com/news-and-blog/blog/2017/01/16/setting-up-qemu-kvm-for-kernel-development/

IMG_FORMAT='qemu-image-%s.img'
KERNEL_FORMAT='../linux/arch/%s/boot/%s'
QEMU_FORMAT='qemu-system-%s'

# This file contains useful function to make a correspondence between a
# debootstrap architecture and the kernel architecture, qemu binary and qemu
# options.
source debootstrap_arch_to.sh

# Set KDB to activate kernel "debugging mode".
# KDB=1

# Set REMOTE if the VM is run through ssh.
# The VM's output will not be redirected into terminal and it will run in
# background.
# REMOTE=1

# Do not use an external window, qemu output is redirected on the terminal.
GRAPHIC='-nographic'

# If we run this script on a remote computer we need to daemonize qemu.
# With this option set, qemu will be daemonized, it is approximately equal to be
# run in background.
if [ ${REMOTE} ]; then
	GRAPHIC='-daemonize'
fi

# If no argument is given, default to amd64 architecture.
debootstrap_arch='amd64'

# Speed up the virtualization by using KVM.
kvm='-enable-kvm'

# If an argument is given, set arch to this.
# If the user gave an unknown architecture, debootstrap_arch_to_kernel_arch will
# fail.
if [ $# -ge 1 ]; then
	debootstrap_arch=$1
fi

# If the guest architecture is different from host, we cannot use KVM.
if [ $(dpkg --print-architecture) != $debootstrap_arch ]; then
	kvm=''
fi

# Call functions to set kernel_arch, qemu_arch, and boot image from
# debootstrap_arch.
kernel_arch=$(debootstrap_arch_to_kernel_arch $debootstrap_arch)
boot_image=$(debootstrap_arch_to_boot_image $debootstrap_arch)
qemu_arch=$(debootstrap_arch_to_qemu_arch $debootstrap_arch)

# For amd64, qemu-system-x86_64 has a default value for -machine option.
# But for armel, this is not the case.
machine=$(debootstrap_arch_to_machine $debootstrap_arch)

# For amd64, qemu-system-x86_64 has a default value for -cpu option.
# But for arm64, this is not the case.
cpu=$(debootstrap_arch_to_cpu $debootstrap_arch)

# Different architectures need different devices.
device=$(debootstrap_arch_to_device $debootstrap_arch)

# Different architectures need different kernel command line.
command_line=$(debootstrap_arch_to_kernel_command_line $debootstrap_arch)

if [ ${KDB} ]; then
	# kgdb will be in another tty and the kernel will run only when kgdb is run.
# 	command_line+=' kgdboc=ttyS1 kgdbwait nokaslr'
	# To use gdbserver/qemu we need to deactivate the kernel address space
	# layout randomization or gdb will not be able to set breakpoint and
	# know where in the code it is.
	command_line+=' nokaslr'
fi

# Set the architectures for the image, the kernel boot image and qemu binary.
printf -v image $IMG_FORMAT $debootstrap_arch
printf -v kernel $KERNEL_FORMAT $kernel_arch $boot_image
printf -v qemu_binary $QEMU_FORMAT $qemu_arch

# -drive file=${image},media=disk,id=hdd,index=0,if=none,cache=none,discard=unmap:
# Use ${image} as disk, id to link with ${device} is hdd, index 0 is the same as
# using -hda, access to it will be done through corresponding ${device}, access
# will not be cached by the host and the drive will support discarding block
# (blocks released by the guest will be release by the host so the image can
# shrink with fstrim).
# Information about unmap were found here:
# https://chrisirwin.ca/posts/discard-with-kvm/
# -virtfs local,path=..,mount_tag=share,security_model=passthrough,readonly,id=share:
# Use a virtfs to share an host directory with the guest.
# The sharing is read only to avoid the guest mess the host files.
# The mount tag is share because in /etc/fstab of the image the tag is share
# and the destination is /root/share.
# Information about qemu directory sharing can be found here:
# https://wiki.qemu.org/Documentation/9psetup
# -smp 4: Give 4 cpu cores to qemu.
# -m 3G: Give 3GB of memory to qemu.
# -net user,hostfwd=tcp::10022-:22:
# This command forward guest ports to host ports.
# The guest port 22 is forwarded to host port 10022, this is mandatory to permit
# ssh connection from the host to the guest.
# -net nic: I think that qemu will add this option by default but giving the
# previous option seems to modify this behavior. Without this option there is no
# internet interface in virtual machine.
# -device virtio-rng-pci: Add a virtio random device which is connected through
# PCI. Add this option permit to produce hardware entropy [and avoid docker to
# block on getrandom()]. The information was found on:
# https://github.com/rapido-linux/rapido/issues/44#issuecomment-393136542
# -device virtio-scsi-pci: This devices permit to use a para-virtualized scsi
# controller. It seems to be mandatory to be able to release blocks freed in
# guest in the host.
# -s: This option is a shorthand to '-gdb tcp::1234' which open a gdbserver on
# TCP port 1234. To connect to it, simply run gdb on host, use file to load what
# you want to debug (e.g. file vmlinux) and connect to gdbserver with 'target
# remote :1234'.
# [-S: This option will not start CPU at VM startup. It can be used to attach
# gdb at the startup of the VM. Then by issuing continue the CPU will start. So
# this option permit debugging from VM startup.]
# -display none: Do not display external display (in the sense that it does not
# open a new window to display qemu output).
# -kernel: Use this kernel in place of distro/image kernel.
# -append: Append this to the kernel command line. WARNING It is better to put
# this option as the last one.
${qemu_binary} \
	-drive file=${image},media=disk,id=hdd,index=0,if=none,cache=none,discard=unmap \
	-virtfs local,path=..,mount_tag=share,security_model=passthrough,readonly,id=share \
	-smp 4 \
	-m 3G \
	-net user,hostfwd=tcp::10022-:22 \
	-net nic \
	-device virtio-rng-pci \
	-device virtio-scsi-pci \
	${device} \
	-s \
	-display none \
	${GRAPHIC} \
	${kvm} \
	${machine} \
	${cpu} \
	-kernel "${kernel}" \
	-append "${command_line}"